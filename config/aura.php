<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your company. This value is used when the
    | framework needs to place the company's name in a navigation bar or
    | any other location as required by the application.
    |
    */

    'company_name' => env('AURA_COMPANY_NAME', 'Aura'),

    /*
    |--------------------------------------------------------------------------
    | Slack Team Identifier
    |--------------------------------------------------------------------------
    |
    | This value is the Slack identifier of your team. This value is used to
    | generate the links of the projects channels for faster navigation
    | between Aura and Slack. If no value is given, only channel name
    | is visible to the user as plain text.
    |
    | To get your team's identifier, navigate to web version of Slack,
    | log into your workspace and copy the first string visible
    | after the /messages/ part of the URI.
    |
    | Example:
    | https://[workspace].slack.com/messages/[team_identifier] <-
    |
    */

    'slack_team_id' => env('AURA_SLACK_TEAM_ID'),

    /*
    |--------------------------------------------------------------------------
    | Debug clients IP addresses
    |--------------------------------------------------------------------------
    |
    | This array stores all addresses that can access debug data.
    | Debug mode involves Laravel Telescope under the /telescope url,
    | and also handy frontend console logs.
    |
    */

    'debug_clients' => [
        '127.0.0.1',
    ],

];
