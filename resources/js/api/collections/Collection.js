import {Collection as BaseCollection} from 'vue-mc'

export default class Collection extends BaseCollection {

    constructor({ data = {}, links = {}, meta = {}, ...additional }) {
        super(data)

        this.links = links
        this.meta = meta
        this.additional = additional
    }
}
