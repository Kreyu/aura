import Collection from '@/api/collections/Collection'
import Project from '@/api/models/Project'

export default class Projects extends Collection {

    model() {
        return Project
    }
}
