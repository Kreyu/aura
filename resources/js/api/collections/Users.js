import Collection from '@/api/collections/Collection'
import User from '@/models/User'

export default class Users extends Collection {

    model() {
        return User
    }
}
