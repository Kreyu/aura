import Collection from '@/api/collections/Collection'
import Activity from '@/api/models/Activity'

export default class Activities extends Collection {

    model() {
        return Activity
    }
}
