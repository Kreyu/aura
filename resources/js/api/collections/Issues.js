import Collection from '@/api/collections/Collection'
import Issue from '@/api/models/Issue'

export default class Issues extends Collection {

    model() {
        return Issue
    }
}
