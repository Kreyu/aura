import Collection from '@/api/collections/Collection'
import ActivityAddition from '@/api/models/ActivityAddition'

export default class ActivityAdditions extends Collection {

    model() {
        return ActivityAddition
    }
}
