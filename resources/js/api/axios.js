import axios from 'axios'
import {App} from '@/app'
import {cacheAdapterEnhancer, throttleAdapterEnhancer} from 'axios-extensions'

const instance = axios.create({
    baseURL: '/api',
    timeout: 5000,
    adapter: throttleAdapterEnhancer(cacheAdapterEnhancer(axios.defaults.adapter)),
    headers: {
        'Cache-Control': 'no-cache',
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRFToken': aura.csrfToken
    }
})

instance.interceptors.request.use(config => {
    App.$Progress.start()
    return config
}, err => Promise.reject(err))

instance.interceptors.response.use(config => {
    App.$Progress.finish()
    return config
}, err => Promise.reject(err))

export default instance
