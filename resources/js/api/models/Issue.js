import Model from '@/api/models/Model'
import User from './User'
import Project from '@/api/models/Project'
import Activities from '@/api/collections/Activities'
import Issues from "@/api/collections/Issues";

export default class Issue extends Model {

    static defaults() {
        return {
            id: null,
            subject: null,
            description: null,
            type: null,
            status: null,
            priority: null,
            project: null,
            owner: null,
            assignee: null,
            parent: null,
            children: null,
            start_date: null,
            due_date: null,
            estimated_time: null,
            done_ratio: null,
            created_at: null,
            updated_at: null,
            activities: null,
        }
    }

    mutations() {
        return {
            owner: (user) => new User(user),
            assignee: (user) => user ? new User(user) : null,
            parent: (parent) => parent ? new Issue(parent) : null,
            children: (children) => new Issues({ data: children }),
            project: (project) => new Project(project),
            activities: (activities) => new Activities({ data: activities })
        }
    }

    hasActivities() {
        return this.activities.length > 0
    }

    hasParent() {
        return this.parent !== null
    }

    hasChildren() {
        return this.children.length > 0
    }
}
