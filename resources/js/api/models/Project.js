import Model from '@/api/models/Model'

export default class Project extends Model {

    static defaults() {
        return {
            id: null,
            name: null,
            description: null,
            identifier: null,
            homepage: null,
            is_public: null,
            inherits_members: null,
            slack_channel: null,
            slack_channel_id: null,
            created_at: null,
            updated_at: null,
            issues_count: null,
        }
    }
}
