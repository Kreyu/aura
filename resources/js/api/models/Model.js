import {Model as BaseModel} from 'vue-mc'

export default class Model extends BaseModel {

    constructor(attributes = {}, collection = null, options = {}) {
        super(attributes, collection, options)
    }
}
