import Model from '@/api/models/Model'
import Activity from '@/api/models/Activity';

export default class ActivityAddition extends Model {

    static defaults() {
        return {
            id: null,
            activity: null,
            property: null,
            value: null
        }
    }

    mutations() {
        return {
            activity: (activity) => new Activity(activity)
        }
    }
}
