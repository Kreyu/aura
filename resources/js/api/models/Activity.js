import Model from '@/api/models/Model'
import User from '@/api/models/User'
import ActivityAdditions from '@/api/collections/ActivityAdditions'

export default class Activity extends Model {

    static defaults() {
        return {
            id: null,
            causer: null,
            subject: null,
            created_at: null,
            updated_at: null,
            additions: null
        }
    }

    mutations() {
        return {
            causer: (user) => new User(user),
            additions: (additions) => new ActivityAdditions({ data: additions })
        }
    }
}
