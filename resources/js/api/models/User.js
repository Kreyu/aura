import Model from '@/api/models/Model'

export default class User extends Model {

    static defaults() {
        return {
            id: null,
            first_name: null,
            middle_name: null,
            last_name: null,
            nick_name: null,
            full_name: null,
            avatar: null,
            email: null,
            email_verified_at: null,
            last_login: null,
            locale: null,
            created_at: null,
            updated_at: null,
            is_owner: false
        }
    }
}
