import axios from './axios'

export default {

    getMe() {
        return axios.get('/me')
    },

    getUsers() {
        return axios.get('/users')
    },

    getUser(id = null) {
        return id ? axios.get('/users/' + id) : this.getMe()
    },

    getProjects() {
        return axios.get('/projects')
    },

    getProject(identifier) {
        return axios.get('/projects/' + identifier)
    },

    getProjectIssues(identifier) {
        return axios.get('/projects/' + identifier + '/issues')
    },

    getProjectActivities(identifier) {
        return axios.get('/projects/' + identifier + '/activities')
    },

    getIssues() {
        return axios.get('/issues')
    },

    getIssue(id) {
        return axios.get('/issues/' + id)
    }
}
