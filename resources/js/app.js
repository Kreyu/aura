require('./bootstrap')

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueTimeago from 'vue-timeago'
import VueProgressBar from 'vue-progressbar'

Vue.use(VueRouter)
Vue.use(VueTimeago)
Vue.use(VueProgressBar)

import Search from '@/components/Search'
import Sidebar from '@/components/Sidebar'
import Notifications from '@/components/Notifications'

Vue.component('aura-search', Search)
Vue.component('aura-sidebar', Sidebar)
Vue.component('aura-notifications', Notifications)

import filters from '@/scripts/filters'

Vue.filter('datetime', filters.datetime)
Vue.filter('humanize', filters.humanize)
Vue.filter('sentence', filters.sentence)
Vue.filter('capitalize', filters.capitalize)

import router from '@/router'
import {mounted} from '@/scripts/hooks'

export const EventBus = new Vue()

export const App = new Vue({
    el: '#aura',
    router,
    mounted() {
        mounted(this)
    }
})
