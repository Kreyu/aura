import Route from 'vue-routisan'
import Dashboard from '@/pages/Dashboard'
import Search from '@/pages/Search'
import ProfileShow from '@/pages/profile/ProfileShow'
import ProjectList from '@/pages/project/ProjectList'
import ProjectShow from '@/pages/project/ProjectShow'
import ProjectIssues from '@/pages/project/ProjectIssues'
import ProjectActivities from '@/pages/project/ProjectActivities'
import ProjectParticipants from '@/pages/project/ProjectParticipants'
import ProjectSettings from '@/pages/project/ProjectSettings'
import ProjectNew from '@/pages/project/ProjectNew'
import ProjectEdit from '@/pages/project/ProjectEdit'
import IssueList from '@/pages/issue/IssueList'
import IssueShow from '@/pages/issue/IssueShow'
import IssueEdit from '@/pages/issue/IssueEdit'
import IssueNew from '@/pages/issue/IssueNew'
import middlewares from '@/http/middlewares'

Route.group({ beforeEnter: middlewares }, () => {
    Route.view('/', Dashboard).name('dashboard')
    Route.view('/search', Search).name('search')

    Route.group({ prefix: '/issues' }, () => {
        Route.view('/', IssueList).name('issue.list')
        Route.view('/new', IssueNew).name('issue.new')
        Route.view('/:id', IssueShow).name('issue.show')
        Route.view('/:id/edit', IssueEdit).name('issue.edit')
    })

    Route.group({ prefix: '/projects' }, () => {
        Route.view('/', ProjectList).name('project.list')
        Route.view('/new', ProjectNew).name('project.new')
        Route.view('/:identifier', ProjectShow).name('project.show')
        Route.view('/:identifier/edit', ProjectEdit).name('project.edit')
        Route.view('/:identifier/issues', ProjectIssues).name('project.issues')
        Route.view('/:identifier/activities', ProjectActivities).name('project.activities')
        Route.view('/:identifier/participants', ProjectParticipants).name('project.participants')
        Route.view('/:identifier/settings', ProjectSettings).name('project.settings')
    })

    Route.group({ prefix: '/profile' }, () => {
        Route.view('/', ProfileShow).name('profile.me')
        Route.view('/:id', ProfileShow).name('profile.show')
        Route.view('/edit', ProjectEdit).name('profile.edit')
    })
})

export default Route.all()
