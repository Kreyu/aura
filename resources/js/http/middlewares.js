/**
 * Update the document title on every route change,
 * relative to the requested route's metadata.
 *
 * @param to
 * @param from
 * @param next
 */
export const updatesTitle = (to, from, next) => {
    if (to.meta.title && to.name && aura.companyName) {
        document.title = `${to.meta.title} - ${aura.companyName}`
    }

    next()
}

/**
 * Registered middlewares to use in the router.
 * Respects the order in the requests flow.
 *
 * @var array
 */
export default [
    updatesTitle
]
