/**
 * Display document contents after the asset loading completes.
 *
 * @param  Vue
 * @return void
 */
export const mounted = (Vue) => {
    document.onreadystatechange = () => {
        if (document.readyState === "complete") {
            Vue.$el.children[1].style.display = 'block'
        }
    }
}
