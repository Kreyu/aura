import moment from 'moment'
import {sentenceCase} from 'change-case'

/**
 * Convert date object to readable format.
 *
 * @param  {object} value - Date object to format
 * @param  {string|null} fallback - Value to display if object cannot be converted to datetime string
 * @return {string}
 */
export const DatetimeFilter = (value, fallback = null) => {
    return value && value.hasOwnProperty('date')
        ? moment(String(value.date)).format('DD/MM/YYYY hh:mm')
        : fallback;
}

/**
 * Convert date object to human-readable string, e.g. 2 days ago.
 *
 * @param  {object} value - Date object to format
 * @param  {string|null} fallback - Value to display if object cannot be converted to datetime string
 * @return {string}
 */
export const HumanizeFilter = (value, fallback = null) => {
    return value && value.hasOwnProperty('date')
        ? moment(String(value.date)).fromNow()
        : fallback;
}

/**
 * Capitalize first letter of the string.
 *
 * @param  {string} string
 * @return {string}
 */
export const CapitalizeFilter = (string) => {
    return string ? string.charAt(0).toUpperCase() + string.slice(1).toLowerCase() : '';
}

/**
 * Convert string to sentence case.
 *
 * @param  {string} string
 * @return {string}
 */
export const SentenceFilter = (string) => {
    return string ? sentenceCase(string) : '';
}

export default {
    datetime: DatetimeFilter,
    humanize: HumanizeFilter,
    sentence: SentenceFilter,
    capitalize: CapitalizeFilter,
}
