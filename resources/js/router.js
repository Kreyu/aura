import VueRouter from 'vue-router'
import routes from '@/http/routes'

export default new VueRouter({
    mode: 'history',
    linkActiveClass: "active",
    routes
})
