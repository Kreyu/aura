window.aura = {
    csrfToken: document.head.querySelector('meta[name="csrf-token"]').content,
    slackTeamId: document.head.querySelector('meta[name="aura-slack-team-id"]').content,
    companyName: document.head.querySelector('meta[name="aura-company-name"]').content
}
