@extends('layouts.app')

@section('title')
    @{{ title }}
@endsection

@section('content')
    <div class="container">
        <transition name="page" mode="out-in">
            <router-view></router-view>
        </transition>
    </div>
@endsection
