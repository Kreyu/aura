<div class="header navbar">
    <div class="header-container">
        <ul class="nav-left">
            <li>
                <a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);">
                    <i class="ti-menu"></i>
                </a>
            </li>

            <aura-search></aura-search>
        </ul>

        <ul class="nav-right">
            <aura-notifications></aura-notifications>

            <li class="dropdown">
                <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                    <div class="peer mR-10">
                        <img class="w-2r bdrs-50p" src="{{ Auth::user()->avatar() }}" alt="User's avatar">
                    </div>

                    <div class="peer mr-2">
                        <span class="fsz-sm c-grey-900">{{ Auth::user()->fullName() }}</span>
                    </div>
                </a>

                <ul class="dropdown-menu fsz-sm">
                    <li>
                        <router-link to="/profile" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                            <i class="ti-user mR-10"></i>
                            <span>Profile</span>
                        </router-link>
                    </li>

                    <li role="separator" class="divider"></li>

                    <li>
                        <a href="{{ url('logout') }}" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                            <i class="ti-power-off mR-10"></i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
