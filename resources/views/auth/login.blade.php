@extends('layouts.auth')

@section('content')
    <h4 class="fw-300 c-grey-900 mB-40">{{ __('Login') }}</h4>

    <form method="POST" action="{{ route('auth.login') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label class="text-normal text-dark">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email"
                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                   value="{{ old('email') }}" required autofocus>

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label class="text-normal text-dark">Password</label>
            <input id="password" type="password"
                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                   name="password" required>

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <div class="peers ai-c jc-sb fxw-nw">
                <div class="peer">
                    <div class="checkbox checkbox-circle checkbox-info peers ai-c">
                        <input type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }} name="inputCheckboxesCall" class="peer">
                        <label for="remember" class="peers peer-greed js-sb ai-c">
                            <span class="peer peer-greed">{{ __('Remember Me') }}</span>
                        </label>
                    </div>
                </div>
                <div class="peer">
                    <button class="btn btn-primary">{{ __('Login') }}</button>

                    {{--<a class="btn btn-link" href="{{ route('auth.password.request') }}">--}}
                        {{--{{ __('Forgot Your Password?') }}--}}
                    {{--</a>--}}
                </div>
            </div>
        </div>
    </form>
@endsection
