@extends('layouts.base')

@section('body')
    <aura-sidebar></aura-sidebar>

    <div class="page-container" style="display: none;">
        @include('partials.navbar')

        <main class="main-content bgc-grey-100">
            <vue-progress-bar></vue-progress-bar>

            <div id="mainContent">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </main>

        @include('partials.footer')
    </div>
@endsection
