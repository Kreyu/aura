<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">

    <title>{{ config('aura.company_name') }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="aura-company-name" content="{{ config('aura.company_name') }}">
    <meta name="aura-slack-team-id" content="{{ config('aura.slack_team_id') }}">

    <link href="{{ asset('images/logo.png') }}" rel="icon">
    <link href="{{ asset('css/adminator.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/vendor.js') }}" defer></script>
    <script src="{{ asset('js/adminator.js') }}" defer></script>
</head>

<body class="app">
<div id="aura">
    @yield('body')
</div>
</body>
</html>
