@component('mail::message')
    Project #{{ $project->id }} was created (by {{ $project->owner->fullName() }})
@endcomponent
