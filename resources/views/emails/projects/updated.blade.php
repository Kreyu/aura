@component('mail::message')
    Project #{{ $project->id }} was updated (by {{ $causer->fullName() }})
@endcomponent
