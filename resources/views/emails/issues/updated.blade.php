@component('mail::message')
    Issue #{{ $issue->id }} was updated (by {{ $causer->fullName() }})
@endcomponent
