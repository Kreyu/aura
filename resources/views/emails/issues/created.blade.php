@component('mail::message')
    Issue #{{ $issue->id }} was created (by {{ $issue->owner->fullName() }})
@endcomponent
