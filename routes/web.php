<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where this application has its registered web routes. These
| routes are loaded by the RouteServiceProvider within a web namespace.
|
*/

Route::name('auth.')->namespace('Auth')->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');

    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');

    Route::prefix('password')->name('password.')->group(function () {
        Route::get('reset', 'ForgotPasswordController@showLinkRequestForm')->name('request');
        Route::post('email', 'ForgotPasswordController@sendResetLinkEmail')->name('email');
        Route::get('reset/{token}', 'ResetPasswordController@showResetForm')->name('reset');
        Route::post('reset', 'ResetPasswordController@reset');
    });
});

Route::prefix('mailable')->name('mailable.')->group(function () {
    Route::prefix('issue')->name('issue.')->group(function () {
        Route::get('created', 'MailableController@issueCreated')->name('created');
        Route::get('updated', 'MailableController@issueUpdated')->name('updated');
    });
});

Route::get('/{url?}', 'VueController@capture')
    ->where('url', '[\/\w\.-]*')
    ->middleware('auth');
