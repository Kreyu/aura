<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
| Here is where this application has its registered api routes. These
| routes are loaded by the RouteServiceProvider within a api namespace.
|
*/

Route::get('me', 'MiscellaneousController@me')->name('me');

Route::prefix('projects')->name('projects.')->group(function () {
    Route::get('/', 'ProjectsController@list')->name('list');
    Route::get('{project}', 'ProjectsController@show')->name('show');
    Route::get('{project}/issues', 'ProjectsController@issues')->name('issues');
    Route::get('{project}/activities', 'ProjectsController@activities')->name('activities');
});

Route::prefix('issues')->name('issues.')->group(function () {
    Route::get('/', 'IssuesController@list')->name('list');
    Route::get('{issue}', 'IssuesController@show')->name('show');
});

Route::prefix('users')->name('users.')->group(function () {
    Route::get('{user}', 'UsersController@show')->name('show');
});
