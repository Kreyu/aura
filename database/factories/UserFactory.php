<?php

use Aura\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'middle_name' => $faker->name,
        'last_name' => $faker->lastName,
        'nick_name' => $faker->userName,
        'locale' => $faker->locale,
        'timezone' => $faker->timezone,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => $faker->dateTimeBetween('-1 month', 'now'),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
