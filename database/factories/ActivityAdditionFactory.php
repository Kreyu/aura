<?php

use Aura\Activity;
use Aura\ActivityAddition;
use Faker\Generator as Faker;

$factory->define(ActivityAddition::class, function (Faker $faker) {
    $activity = Activity::drawOne();
    $subjectClass = get_class($activity->subject);

    $columns = array_intersect(
        $subjectClass::$additions,
        $subjectClass::columns()
    );

    $property = $columns[array_rand($columns)];

    return [
        'activity_id' => $activity->id,
        'property' => $property,
        'value' => $faker->word
    ];
});
