<?php

use Aura\Enums\IssuePriority;
use Aura\Enums\IssueStatus;
use Aura\Enums\IssueType;
use Aura\Issue;
use Aura\Project;
use Aura\User;
use Faker\Generator as Faker;

$factory->define(Issue::class, function (Faker $faker) {
    $assignee = $faker->boolean ? User::drawOne() : null;
    $project = Project::drawOne();
    $owner = User::drawOne();

    return [
        'subject' => $faker->sentence(5),
        'description' => $faker->text,
        'type' => IssueType::getRandomValue(),
        'status' => IssueStatus::getRandomValue(),
        'priority' => IssuePriority::getRandomValue(),
        'project_id' => $project,
        'owner_id' => $owner,
        'assignee_id' => $assignee,
        'start_date' => $faker->dateTimeBetween('-1 year', '-2 months'),
        'due_date' => $faker->boolean ? $faker->dateTimeBetween('-1 month', 'now') : null,
        'estimated_time' => $faker->numberBetween(0.5, 100),
        'done_ratio' => $faker->numberBetween(0, 10) * 10,
    ];
});
