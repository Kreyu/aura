<?php

use Aura\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    $hasSlack = $faker->boolean;

    return [
        'name' => $faker->company,
        'description' => $faker->text,
        'identifier' => $faker->slug,
        'homepage' => $faker->url,
        'is_public' => $faker->boolean,
        'inherits_members' => $faker->boolean,
        'slack_channel' => $hasSlack ? $faker->domainWord : null,
        'slack_channel_id' => $hasSlack && $faker->boolean ? str_random(9) : null,
    ];
});
