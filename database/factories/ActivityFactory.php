<?php

use Aura\Activity;
use Aura\Issue;
use Aura\Project;
use Aura\User;
use Faker\Generator as Faker;

$factory->define(Activity::class, function (Faker $faker) {
    $causer = User::drawOne();
    $subjectType = $faker->boolean ? Issue::class : Project::class;
    $subject = $subjectType::drawOne();

    return [
        'causer_id' => $causer->id,
        'subject_id' => $subject->id,
        'subject_type' => $subjectType::$alias,
        'content' => $faker->text,
    ];
});
