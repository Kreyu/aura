<?php

use Aura\ActivityAddition;
use Illuminate\Database\Seeder;

class ActivityAdditionsTableSeeder extends Seeder
{
    /**
     * Run the table seeder.
     *
     * @return void
     */
    public function run()
    {
        factory(ActivityAddition::class, 250)->create();
    }
}
