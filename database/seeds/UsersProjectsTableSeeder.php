<?php

use Aura\Project;
use Aura\User;
use Aura\UserProject;
use Illuminate\Database\Seeder;

class UsersProjectsTableSeeder extends Seeder
{
    /**
     * Run the table seeder.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $projects = Project::all();

        foreach ($users as $user) {
            $userProjects = $projects->random(5);

            foreach ($userProjects as $project) {
                UserProject::create([
                    'user_id' => $user->id,
                    'project_id' => $project->id,
                ]);
            }
        }
    }
}
