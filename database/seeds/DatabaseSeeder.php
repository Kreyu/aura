<?php

use Aura\Activity;
use Aura\ActivityAddition;
use Aura\User;
use Aura\Issue;
use Aura\Project;
use Aura\UserProject;
use Aura\Helpers\Database;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->truncate();

        $this->call(UsersTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(IssuesTableSeeder::class);
        $this->call(UsersProjectsTableSeeder::class);
        $this->call(ActivitiesTableSeeder::class);
        $this->call(ActivityAdditionsTableSeeder::class);

        $this->showCredentials();

        Model::reguard();
    }

    /**
     * Truncate tables with disabled foreign key checks.
     *
     * @return void
     */
    private function truncate()
    {
        Database::disableForeignKeyChecks();

        foreach ($this->tablesToTruncate() as $table) {
            DB::table($table)->truncate();
        }

        Database::enableForeignKeyChecks();
    }

    /**
     * Get an array of tables to truncate.
     *
     * @return array
     */
    private function tablesToTruncate()
    {
        return [
            UserProject::getTableName(),
            User::getTableName(),
            Project::getTableName(),
            Issue::getTableName(),
            ActivityAddition::getTableName(),
            Activity::getTableName(),
        ];
    }

    /**
     * Show auth credentials of random generated user,
     * for easier testing during development.
     *
     * @return void
     */
    private function showCredentials()
    {
        $user = User::drawOne();
        $credentials = "{$user->email} / secret";

        $this->command->getOutput()->writeln("<info>Development credentials:</info> {$credentials}");
    }
}
