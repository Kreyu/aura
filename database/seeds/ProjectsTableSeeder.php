<?php

use Aura\Project;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the table seeder.
     *
     * @return void
     */
    public function run()
    {
        factory(Project::class, 10)->create();
    }
}
