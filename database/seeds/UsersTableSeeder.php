<?php

use Aura\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the table seeder.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 5)->create();
    }
}
