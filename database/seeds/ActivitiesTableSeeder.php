<?php

use Aura\Activity;
use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the table seeder.
     *
     * @return void
     */
    public function run()
    {
        factory(Activity::class, 50)->create();
    }
}
