<?php

use Aura\Issue;
use Illuminate\Database\Seeder;

class IssuesTableSeeder extends Seeder
{
    /**
     * Quantity of the records to create.
     *
     * @var int
     */
    protected $quantity = 50;

    /**
     * Run the table seeder.
     *
     * @return void
     */
    public function run()
    {
        factory(Issue::class, $this->quantity)->create()->each(function (Issue $issue) {
            $hasParent = (bool) rand(0, 1);

            $parentId = rand(1, $this->quantity);
            $isUnique = !in_array($parentId, $issue->children->pluck('id')->toArray());

            $issue->parent_id = $hasParent && $isUnique
                ? $parentId : null;

             $issue->save();
        });
    }
}
