<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIssuesTable extends Migration
{
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->text('description')->nullable();
            $table->unsignedInteger('type');
            $table->unsignedInteger('status');
            $table->unsignedInteger('priority');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('owner_id');
            $table->unsignedInteger('assignee_id')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('due_date')->nullable();
            $table->decimal('estimated_time')->nullable();
            $table->unsignedInteger('done_ratio')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('issues');
    }
}
