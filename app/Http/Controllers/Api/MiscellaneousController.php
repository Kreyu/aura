<?php

namespace Aura\Http\Controllers\Api;

use Aura\Http\Controllers\Controller;
use Aura\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Auth;

class MiscellaneousController extends Controller
{
    public function me()
    {
        return new UserResource(Auth::user());
    }
}
