<?php

namespace Aura\Http\Controllers\Api;

use Aura\Http\Controllers\Controller;
use Aura\Http\Resources\User as UserResource;
use Aura\User;

class UsersController extends Controller
{
    public function show(User $user)
    {
        return new UserResource($user);
    }
}
