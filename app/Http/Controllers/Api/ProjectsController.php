<?php

namespace Aura\Http\Controllers\Api;

use Aura\Http\Controllers\Controller;
use Aura\Http\Resources\ActivityCollection;
use Aura\Http\Resources\IssueCollection;
use Aura\Http\Resources\Project as ProjectResource;
use Aura\Http\Resources\ProjectCollection;
use Aura\Project;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    public function list()
    {
        return new ProjectCollection(
            Auth::user()->accessibleProjects()->paginate(5)
        );
    }

    public function show(Project $project)
    {
        return new ProjectResource($project);
    }

    public function issues(Project $project)
    {
        return new IssueCollection(
            $project->issues()->paginate(5),
            $project
        );
    }

    public function activities(Project $project)
    {
        return new ActivityCollection(
            $project->activities()->paginate(10),
            $project
        );
    }
}
