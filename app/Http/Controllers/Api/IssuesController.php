<?php

namespace Aura\Http\Controllers\Api;

use Aura\Http\Controllers\Controller;
use Aura\Http\Resources\Issue as IssueResource;
use Aura\Http\Resources\IssueCollection;
use Aura\Issue;
use Illuminate\Support\Facades\Auth;

class IssuesController extends Controller
{
    public function list()
    {
        return new IssueCollection(
            Auth::user()->issues()->paginate(5)
        );
    }

    public function show(Issue $issue)
    {
        return new IssueResource($issue);
    }
}
