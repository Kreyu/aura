<?php

namespace Aura\Http\Controllers\Web;

use Aura\Http\Controllers\Controller;
use Aura\Issue;
use Aura\Mail\IssueCreated;
use Aura\Mail\IssueUpdated;
use Aura\User;

class MailableController extends Controller
{
    public function issueCreated()
    {
        $issue = Issue::drawOne();

        return new IssueCreated($issue);
    }

    public function issueUpdated()
    {
        $issue = Issue::drawOne();
        $causer = User::drawOne();

        return new IssueUpdated($issue, $causer);
    }
}
