<?php

namespace Aura\Http\Controllers\Web;

use Aura\Http\Controllers\Controller;

class VueController extends Controller
{
    public function capture()
    {
        return view('vue');
    }
}
