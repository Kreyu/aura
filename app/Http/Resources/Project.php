<?php

namespace Aura\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Project extends JsonResource
{
    /**
     * Transform the resource into a json.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'identifier' => $this->identifier,
            'homepage' => $this->homepage,
            'is_public' => $this->is_public,
            'inherits_members' => $this->inherits_members,
            'slack_channel' => $this->slack_channel,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'issues_count' => $this->issues_count,
        ];
    }
}
