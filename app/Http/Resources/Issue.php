<?php

namespace Aura\Http\Resources;

use Aura\Enums\IssuePriority;
use Aura\Enums\IssueStatus;
use Aura\Enums\IssueType;
use Illuminate\Http\Resources\Json\JsonResource;

class Issue extends JsonResource
{
    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @return void
     */
    public function __construct($resource, $parent = null)
    {
        parent::__construct($resource);

        $this->parent = $parent;
    }

    /**
     * Transform the resource into a json.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $parent = $this->parent()->first();

        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $this->description,
            'type' => IssueType::getDescription($this->type),
            'status' => IssueStatus::getDescription($this->status),
            'priority' => IssuePriority::getDescription($this->priority),
            'project' => $this->project,
            'owner' => new User($this->owner),
            'assignee' => new User($this->assignee),
            'parent' => $parent ? new self($parent, $parent) : null,
            'start_date' => $this->start_date,
            'due_date' => $this->due_date,
            'estimated_time' => $this->estimated_time,
            'done_ratio' => $this->done_ratio,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activities' => new ActivityCollection($this->activities),
        ];
    }

    public function with($request)
    {
        $children = $this->parent
            ? $this->children->except($this->parent)
            : $this->children;

        return [
            'data' => [
                'children' => new IssueCollection($children),
            ],
        ];
    }
}
