<?php

namespace Aura\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Activity extends JsonResource
{
    /**
     * Transform the resource into a json.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $subjectClass = get_class($this->subject);

        return [
            'id' => $this->id,
            'causer' => new User($this->causer),
            'content' => $this->content,
            'subject' => $this->subject,
            'subject_class' => $subjectClass,
            'additions' => $this->additions,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
