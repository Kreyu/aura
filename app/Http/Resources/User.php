<?php

namespace Aura\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class User extends JsonResource
{
    /**
     * Transform the resource into a json.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'nick_name' => $this->nick_name,
            'full_name' => implode(' ', [$this->first_name, $this->middle_name, $this->last_name]),
            'avatar' => $this->avatar(),
            'email' => $this->email,
            'email_verified_at' => $this->email_verified_at,
            'last_login' => $this->last_login,
            'locale' => $this->locale,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_owner' => Auth::user()->id === $this->id
        ];
    }
}
