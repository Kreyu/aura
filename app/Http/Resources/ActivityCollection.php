<?php

namespace Aura\Http\Resources;

use Aura\Project;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ActivityCollection extends ResourceCollection
{
    /**
     * @var Project|null
     */
    protected $project;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param  Project|null $project
     * @return void
     */
    public function __construct($resource, ?Project $project = null)
    {
        parent::__construct($resource);

        $this->project = $project;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function with($request)
    {
        return [
            'project' => $this->project
        ];
    }
}
