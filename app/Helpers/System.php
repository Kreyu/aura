<?php

namespace Aura\Helpers;

use Aura\Exceptions\DatabaseException;
use Dotenv\Dotenv;
use Illuminate\Foundation\Bootstrap\LoadConfiguration;
use Illuminate\Support\Facades\Artisan;

class System
{
    /**
     * Ensure the application's environment file exists.
     *
     * @return void
     */
    public static function checkEnvironmentFile()
    {
        $envFile = app()->environmentFilePath();

        if (!file_exists($envFile)) {
            copy("{$envFile}.example", $envFile);
            static::reloadEnvironmentFile();
        }
    }

    /**
     * Ensure the application's key exists.
     *
     * @return void
     */
    public static function checkApplicationKey()
    {
        if (!env('APP_KEY')) {
            Artisan::call('key:generate');
        }
    }

    /**
     * Reload the environment file to ensure its in use.
     *
     * @return void
     */
    public static function reloadEnvironmentFile()
    {
        with(new Dotenv(app()->environmentPath(), app()->environmentFile()))->overload();
        with(new LoadConfiguration())->bootstrap(app());
    }

    /**
     * Set environment file value.
     *
     * @param  string $key
     * @param  string $value
     * @return void
     */
    public static function setEnvironmentValue($key, $value)
    {
        $envFile = app()->environmentFilePath();

        System::checkEnvironmentFile();

        $str = file_get_contents($envFile);

        $oldValue = env($key);
        $str = str_replace("{$key}={$oldValue}", "{$key}={$value}", $str);

        $fp = fopen($envFile, 'w');

        fwrite($fp, $str);
        fclose($fp);
    }

    /**
     * Create database schema if not exists.
     *
     * @return void
     * @throws DatabaseException
     */
    public static function createDatabaseIfNotExists()
    {
        $database = env('DB_DATABASE', false);

        if (!$database) {
            throw new DatabaseException('Skipping creation of database as env(DB_DATABASE) is empty.');
        }

        try {
            $pdo = System::getPDOConnection(
                env('DB_HOST'),
                env('DB_PORT'),
                env('DB_USERNAME'),
                env('DB_PASSWORD')
            );

            $pdo->exec("CREATE DATABASE IF NOT EXISTS {$database};");
        } catch (\PDOException $exception) {
            throw new DatabaseException($exception->getMessage());
        }
    }

    /**
     * Get instance of a newly created PDO connection.
     *
     * @param  string $host
     * @param  string $port
     * @param  string $username
     * @param  string $password
     * @return \PDO
     */
    public static function getPDOConnection($host, $port, $username, $password)
    {
        return new \PDO(sprintf('mysql:host=%s;port=%d;', $host, $port), $username, $password);
    }
}
