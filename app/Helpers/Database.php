<?php

namespace Aura\Helpers;

use Illuminate\Support\Facades\DB;

class Database
{
    /**
     * Disable foreign key checks globally.
     *
     * @return void
     */
    public static function disableForeignKeyChecks()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
    }

    /**
     * Enable foreign key checks globally.
     *
     * @return void
     */
    public static function enableForeignKeyChecks()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
