<?php

namespace Aura;

use Aura\Contracts\ActivitySubject;
use Aura\Database\Eloquent\ActivitiableModel;
use Aura\Database\Eloquent\Resource;
use Aura\Http\Resources\Issue as IssueResource;
use Aura\Http\Resources\IssueCollection;
use Aura\Scopes\LatestScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Issue model.
 *
 * @property int $id
 * @property string $subject
 * @property string|null $description
 * @property int $type
 * @property int $status
 * @property int $priority
 * @property int $project_id
 * @property int $owner_id
 * @property int|null $assignee_id
 * @property int|null $parent_id
 * @property Carbon $start_date
 * @property Carbon|null $due_date
 * @property int|null $estimated_time
 * @property int $done_ratio
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Activity[] $activities
 * @property-read \Aura\User $assignee
 * @property-read Collection|Issue[] $children
 * @property-read User $owner
 * @property-read Issue $parent
 * @property-read Project $project
 * @mixin \Eloquent
 */
class Issue extends ActivitiableModel implements ActivitySubject
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_date', 'due_date'];

    /**
     * The resource class of the model.
     *
     * @var string
     */
    protected $resource = IssueResource::class;

    /**
     * The collection class of the model.
     *
     * @var string
     */
    protected $collection = IssueCollection::class;

    /**
     * The alias used in polymorphic relations.
     *
     * @var string
     */
    public static $alias = 'issues';

    /**
     * The attributes that should be logged as activity additions.
     *
     * @var array
     */
    public static $additions = [
        'status', 'priority', 'assignee',
        'start_date', 'due_date', 'done_ratio'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LatestScope);
    }

    /**
     * Get the owner of the issue.
     *
     * @return BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the user assigned to the issue.
     *
     * @return BelongsTo
     */
    public function assignee()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the project that the issue belongs to.
     *
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the parent issue of the issue.
     *
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo($this, 'parent_id');
    }

    /**
     * Get the children issues for the issue.
     *
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany($this, 'parent_id');
    }

    /**
     * Get the activities logged for the project.
     *
     * @return MorphMany
     */
    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }
}
