<?php

namespace Aura;

use Aura\Contracts\ActivitySubject;
use Aura\Database\Eloquent\ActivitiableModel;
use Aura\Scopes\LatestScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Activity model.
 *
 * @property int $id
 * @property int $causer_id
 * @property int $subject_id
 * @property string $subject_type
 * @property string $content
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|ActivityAddition[] $additions
 * @property-read ActivitySubject $subject
 * @property-read User $causer
 * @mixin \Eloquent
 */
class Activity extends ActivitiableModel
{
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['additions'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LatestScope);
    }

    /**
     * Get the causer of the activity.
     *
     * @return BelongsTo
     */
    public function causer()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the additions of the activity.
     *
     * @return HasMany
     */
    public function additions()
    {
        return $this->hasMany(ActivityAddition::class);
    }

    /**
     * Get all of the activity subjectable models.
     *
     * @return BelongsTo
     */
    public function subject()
    {
        return $this->morphTo();
    }
}
