<?php

namespace Aura\Events;

use Aura\Project;
use Aura\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ProjectUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The project related with the event.
     *
     * @var Project
     */
    public $project;

    /**
     * The user that caused the event to fire.
     *
     * @var User
     */
    public $causer;

    public function __construct(Project $project, User $causer)
    {
        $this->project = $project;
        $this->causer = $causer;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('projects');
    }
}
