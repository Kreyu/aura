<?php

namespace Aura\Events;

use Aura\Issue;
use Aura\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class IssueUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The issue related with the event.
     *
     * @var Issue
     */
    public $issue;

    /**
     * The user that caused the event to fire.
     *
     * @var User
     */
    public $causer;

    public function __construct(Issue $issue, User $causer)
    {
        $this->issue = $issue;
        $this->causer = $causer;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PrivateChannel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('issues');
    }
}
