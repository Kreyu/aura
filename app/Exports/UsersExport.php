<?php

namespace Aura\Exports;

use Aura\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    /**
     * Get collections of data to export.
     *
     * @return Collection
     */
    public function collection()
    {
        return User::all();
    }
}
