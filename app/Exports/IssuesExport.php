<?php

namespace Aura\Exports;

use Aura\Issue;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class IssuesExport implements FromCollection
{
    /**
     * Get collections of data to export.
     *
     * @return Collection
     */
    public function collection()
    {
        return Issue::all();
    }
}
