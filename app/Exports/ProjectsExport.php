<?php

namespace Aura\Exports;

use Aura\Project;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProjectsExport implements FromCollection
{
    /**
     * Get collections of data to export.
     *
     * @return Collection
     */
    public function collection()
    {
        return Project::all();
    }
}
