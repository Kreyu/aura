<?php

namespace Aura;

use Aura\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * ActivityAddition model.
 *
 * @property int $id
 * @property string $property
 * @property string $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Activity $acitivity
 * @mixin \Eloquent
 */
class ActivityAddition extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the activity of the addition.
     *
     * @return BelongsTo
     */
    public function acitivity()
    {
        return $this->belongsTo(Activity::class);
    }
}
