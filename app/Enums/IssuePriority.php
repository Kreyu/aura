<?php

namespace Aura\Enums;

use BenSampo\Enum\Enum;

class IssuePriority extends Enum
{
    const Low = 0;
    const Normal = 1;
    const High = 2;
    const Urgent = 3;
    const Immediate = 4;
    const Postponed = 5;
    const Background = 6;
}
