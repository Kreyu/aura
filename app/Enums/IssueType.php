<?php

namespace Aura\Enums;

use BenSampo\Enum\Enum;

class IssueType extends Enum
{
    const Bug = 0;
    const Feature = 1;
    const Support = 2;
}
