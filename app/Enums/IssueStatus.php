<?php

namespace Aura\Enums;

use BenSampo\Enum\Enum;

class IssueStatus extends Enum
{
    const New = 0;
    const Assigned = 1;
    const Resolved = 2;
    const Feedback = 3;
    const Closed = 4;
    const Rejected = 5;
}
