<?php

namespace Aura\Console\Commands\System;

use Aura\Exceptions\DatabaseException;
use Aura\Helpers\System;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class VerifyApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aura:verify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check all required .env parameters and database.';

    /**
     * Array of required environment parameters to check.
     *
     * @var array
     */
    protected $requiredParameters = [
        'DB_HOST' => 'Database host',
        'DB_PORT' => 'Database port',
        'DB_DATABASE' => 'Database table name',
        'DB_USERNAME' => 'Database username',
        'DB_PASSWORD' => 'Database password',
        'MAIL_DRIVER' => 'Mail driver',
        'MAIL_HOST' => 'Mail host',
        'MAIL_PORT' => 'Mail port',
        'MAIL_USERNAME' => 'Mail username',
        'MAIL_PASSWORD' => 'Mail password',
        'MAIL_ENCRYPTION' => 'Mail encryption',
        'AURA_COMPANY_NAME' => 'Company name',
        'AURA_SLACK_TEAM_ID' => 'Slack team identifier',
        'APP_URL' => 'Application URL',
    ];

    /**
     * Array of default parameter values.
     *
     * @var array
     */
    protected $defaultValues = [
        'DB_HOST' => '127.0.0.1',
        'DB_PORT' => 3306,
        'DB_DATABASE' => 'aura',
        'DB_USERNAME' => 'root',
    ];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        System::checkEnvironmentFile();
        System::checkApplicationKey();
        System::reloadEnvironmentFile();

        foreach ($this->requiredParameters as $key => $question) {
            if (!env($key)) {
                $value = $this->ask($question, array_get($this->defaultValues, $key));
                System::setEnvironmentValue($key, $value);
            }
        }

        System::reloadEnvironmentFile();

        try {
            System::createDatabaseIfNotExists();
        } catch (DatabaseException $exception) {
            $this->error($exception->getMessage());
            return;
        }

        $this->call('migrate');

        $this->info('Aura application successfully verified!');
    }

    public static function invoke()
    {
        with(new static)->call('aura:verify');
    }
}
