<?php

namespace Aura\Providers;

use Aura\Activity;
use Aura\Issue;
use Aura\Observers\ActivityObserver;
use Aura\Project;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Morphings
        Relation::morphMap([
            Issue::$alias => Issue::class,
            Project::$alias => Project::class,
        ]);

        // Observers
        Activity::observe(ActivityObserver::class);
    }
}
