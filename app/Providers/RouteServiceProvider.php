<?php

namespace Aura\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Aura\Http\Controllers';

    /**
     * This namespace is applied to web controller routes.
     *
     * @var string
     */
    protected $webNamespace = 'Aura\Http\Controllers\Web';

    /**
     * This namespace is applied to api controller routes.
     *
     * @var string
     */
    protected $apiNamespace = 'Aura\Http\Controllers\Api';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are not stateless, works same as web routes,
     * except being accessible only by XHR requests, returning json resources.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::middleware('web')
            ->namespace($this->apiNamespace)
            ->prefix('api')
            ->name('api.')
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->webNamespace)
            ->group(base_path('routes/web.php'));
    }
}
