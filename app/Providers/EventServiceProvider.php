<?php

namespace Aura\Providers;

use Aura\Listeners\IssueSubscriber;
use Aura\Listeners\SaveLastLogin;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Login::class => [
            SaveLastLogin::class,
        ],
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * The event subscriber mappings for the application.
     *
     * @var array
     */
    protected $subscribe = [
        IssueSubscriber::class,
    ];
}
