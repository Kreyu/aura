<?php

namespace Aura\Observers;

use Aura\Events\IssueCreated;
use Aura\Events\IssueUpdated;
use Aura\Issue;
use Auth;

class IssueObserver
{
    /**
     * Handle the Issue "created" event.
     *
     * @param  Issue $issue
     * @return void
     */
    public function created(Issue $issue)
    {
        event(new IssueCreated($issue));
    }

    /**
     * Handle the Issue "updated" event.
     *
     * @param  Issue $issue
     * @return void
     */
    public function updated(Issue $issue)
    {
        event(new IssueUpdated($issue, Auth::user()));
    }
}
