<?php

namespace Aura\Observers;

use Aura\Events\ProjectCreated;
use Aura\Events\ProjectUpdated;
use Aura\Project;
use Auth;

class ProjectObserver
{
    /**
     * Handle the Project "created" event.
     *
     * @param  Project $project
     * @return void
     */
    public function created(Project $project)
    {
        event(new ProjectCreated($project));
    }

    /**
     * Handle the Issue "updated" event.
     *
     * @param  Project $project
     * @return void
     */
    public function updated(Project $project)
    {
        event(new ProjectUpdated($project, Auth::user()));
    }
}
