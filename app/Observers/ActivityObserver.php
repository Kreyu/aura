<?php

namespace Aura\Observers;

use Aura\Activity;
use Aura\Issue;
use Aura\Project;

class ActivityObserver
{
    /**
     * Handle the Activity "created" event.
     *
     * @param  Activity $activity
     * @return void
     */
    public function created(Activity $activity)
    {
        /**
         * Subject of the activity.
         *
         * @var Project|Issue|null $subject
         */
        $subject = $activity->subject()->get();
    }
}
