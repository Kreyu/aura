<?php

namespace Aura;

use Aura\Auth\User as Authenticatable;
use Aura\Contracts\ActivityCauser;
use Aura\Database\Traits\TimezoneAware;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Laravolt\Avatar\Avatar;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

/**
 * User model.
 *
 * @property int $id
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $nick_name
 * @property string $email
 * @property string $password
 * @property string $locale
 * @property string $timezone
 * @property Carbon|null $last_login
 * @property Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Project[] $accessibleProjects
 * @property-read Collection|Issue[] $issuesAssigned
 * @property-read Collection|Issue[] $issuesReported
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail, ActivityCauser
{
    use Notifiable, HasRelationships, TimezoneAware;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['last_login'];

    /**
     * Get the issues accessible by the user.
     *
     * @return HasManyDeep
     */
    public function issues()
    {
        return $this->hasManyDeep(
            Issue::class, [
                'users_projects',
                Project::class
            ]
        );
    }

    /**
     * Get the issues assigned to the user.
     *
     * @return HasMany
     */
    public function issuesAssigned()
    {
        return $this->hasMany(Issue::class)
            ->where('assignee_id', $this->id);
    }

    /**
     * Get the issues reported by the user.
     *
     * @return HasMany
     */
    public function issuesReported()
    {
        return $this->hasMany(Issue::class)
            ->where('owner_id', $this->id);
    }

    /**
     * Get the projects accessible by the user.
     *
     * @return BelongsToMany
     */
    public function accessibleProjects()
    {
        return $this->belongsToMany(Project::class, 'users_projects')
            ->using(UserProject::class);
    }

    /**
     * Get the avatar image link for the user.
     *
     * @return string
     */
    public function avatar()
    {
        return with(new Avatar())
            ->create($this->fullName())
            ->toBase64();
    }

    /**
     * Get the concatenated full name of the user.
     *
     * @return string
     */
    public function fullName()
    {
        return implode(' ', [
            $this->first_name,
            $this->middle_name,
            $this->last_name
        ]);
    }
}
