<?php

namespace Aura\Listeners;

use Aura\Events\IssueCreated;
use Aura\Events\IssueUpdated;
use Aura\Mail\IssueCreated as IssueCreatedMail;
use Aura\Mail\IssueUpdated as IssueUpdatedMail;
use Aura\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;

class IssueSubscriber implements ShouldQueue
{
    /**
     * Handle issue creation events.
     *
     * @param  IssueCreated $event
     * @return void
     */
    public function onCreate(IssueCreated $event)
    {
        $issue = $event->issue;

        $recipents = $this->getRecipients(
            $issue->project()->participants,
            $issue->owner
        );

        Mail::bcc($recipents)
            ->send(new IssueCreatedMail($issue));
    }

    /**
     * Handle issue update events.
     *
     * @param  IssueUpdated $event
     * @return void
     */
    public function onUpdate(IssueUpdated $event)
    {
        $issue = $event->issue;
        $causer = $event->causer;

        $recipents = $this->getRecipients(
            $issue->project()->participants,
            $causer
        );

        Mail::bcc($recipents)
            ->send(new IssueUpdatedMail($issue, $causer));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher $events
     * @return void
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            IssueCreated::class,
            __CLASS__ . '@onCreate'
        );

        $events->listen(
            IssueUpdated::class,
            __CLASS__ . '@onUpdate'
        );
    }

    /**
     * Get array of the mailable recipients emails.
     *
     * @param  Collection $participants
     * @param  User|null $except
     * @return array
     */
    private function getRecipients(Collection $participants, User $except = null): array
    {
        return $participants->except($except)
            ->pluck('email');
    }
}
