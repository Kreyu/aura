<?php

namespace Aura\Listeners;

use Aura\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaveLastLogin implements ShouldQueue
{
    /**
     * Handle the user login event.
     *
     * @param  Login $event
     * @return void
     */
    public function handle(Login $event)
    {
        /**
         * User that logged into the application.
         *
         * @var User $user
         */
        $user = $event->user;

        $user->last_login = now()->format('Y-m-d H:i:s');
        $user->save();
    }
}
