<?php

namespace Aura\Listeners;

use Aura\Events\ProjectCreated;
use Aura\Events\ProjectUpdated;
use Aura\Mail\ProjectCreated as ProjectCreatedEmail;
use Aura\Mail\ProjectUpdated as ProjectUpdatedEmail;
use Aura\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;

class ProjectSubscriber implements ShouldQueue
{
    /**
     * Handle project creation events.
     *
     * @param  ProjectCreated $event
     * @return void
     */
    public function onCreate(ProjectCreated $event)
    {
        $project = $event->project;

        $recipents = $this->getRecipients(
            $project->participants
        );

        Mail::bcc($recipents)
            ->send(new ProjectCreatedEmail($project));
    }

    /**
     * Handle project update events.
     *
     * @param  ProjectUpdated $event
     * @return void
     */
    public function onUpdate(ProjectUpdated $event)
    {
        $project = $event->project;
        $causer = $event->causer;

        $recipents = $this->getRecipients(
            $project->participants,
            $causer
        );

        Mail::bcc($recipents)
            ->send(new ProjectUpdatedEmail($project, $causer));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher $events
     * @return void
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            ProjectCreated::class,
            __CLASS__ . '@onCreate'
        );

        $events->listen(
            ProjectUpdated::class,
            __CLASS__ . '@onUpdate'
        );
    }

    /**
     * Get array of the mailable recipients emails.
     *
     * @param  Collection $participants
     * @param  User|null $except
     * @return array
     */
    private function getRecipients(Collection $participants, User $except = null): array
    {
        return $participants->except($except)
            ->pluck('email');
    }
}
