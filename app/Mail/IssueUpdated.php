<?php

namespace Aura\Mail;

use Aura\Issue;
use Aura\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class IssueUpdated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The issue available from the mailable.
     *
     * @var Issue
     */
    public $issue;

    /**
     * The user available from the mailable.
     *
     * @var User
     */
    public $causer;

    public function __construct(Issue $issue, User $causer)
    {
        $this->issue = $issue;
        $this->causer = $causer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.issues.updated');
    }
}
