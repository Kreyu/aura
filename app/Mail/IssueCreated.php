<?php

namespace Aura\Mail;

use Aura\Issue;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class IssueCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The issue available from the mailable.
     *
     * @var Issue
     */
    public $issue;

    public function __construct(Issue $issue)
    {
        $this->issue = $issue;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.issues.created');
    }
}
