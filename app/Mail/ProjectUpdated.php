<?php

namespace Aura\Mail;

use Aura\Project;
use Aura\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProjectUpdated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The project available from the mailable.
     *
     * @var Project
     */
    public $project;

    /**
     * The user available from the mailable.
     *
     * @var User
     */
    public $causer;

    public function __construct(Project $project, User $causer)
    {
        $this->project = $project;
        $this->causer = $causer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.projects.updated');
    }
}
