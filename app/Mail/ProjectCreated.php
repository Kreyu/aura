<?php

namespace Aura\Mail;

use Aura\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProjectCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The project available from the mailable.
     *
     * @var Project
     */
    public $project;

    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.projects.created');
    }
}
