<?php

namespace Aura\Database\Eloquent;

/**
 * Resource model.
 *
 * @mixin \Eloquent
 */
abstract class ResourcefulModel extends Model
{
    /**
     * The resource class of the model.
     *
     * @var string|null
     */
    protected $resource = null;

    /**
     * The collection class of the model.
     *
     * @var string|null
     */
    protected $collection = null;

    /**
     * Get resource class of the model.
     *
     * @return null|string
     */
    public static function resource()
    {
        return with(new static)->resource;
    }

    /**
     * Get collection class of the model.
     *
     * @return null|string
     */
    public static function collection()
    {
        return with(new static)->collection;
    }

    /**
     * Get resource class of the model.
     *
     * @return null|string
     */
    public function getResourceClass()
    {
        return $this->resource;
    }

    /**
     * Get collection class of the model.
     *
     * @return null|string
     */
    public function getCollectionClass()
    {
        return $this->collection;
    }
}
