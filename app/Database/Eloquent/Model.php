<?php

namespace Aura\Database\Eloquent;

use Aura\Traits\EloquentHelpers;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Support\Facades\Schema;

/**
 * Base application model.
 *
 * @method static create(array $attributes = [])
 * @method static save(array $options = [])
 * @mixin  \Eloquent
 */
abstract class Model extends BaseModel
{
    use EloquentHelpers;

    /**
     * Get the column listing of the model's table.
     *
     * @return array
     */
    public static function columns()
    {
        return with(new static)->getColumnListing();
    }

    /**
     * Get the column listing of the model's table.
     *
     * @return array
     */
    public function getColumnListing()
    {
        return Schema::getColumnListing(self::getTableName());
    }
}
