<?php

namespace Aura\Database\Eloquent\Relations;

use Aura\Traits\EloquentHelpers;
use Illuminate\Database\Eloquent\Relations\Pivot as BasePivot;

/**
 * Base application pivot model.
 *
 * @method static create(array $attributes = [])
 * @method static save(array $options = [])
 * @mixin  \Eloquent
 */
class Pivot extends BasePivot
{
    use EloquentHelpers;
}
