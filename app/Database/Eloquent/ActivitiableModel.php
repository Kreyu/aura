<?php

namespace Aura\Database\Eloquent;

/**
 * Activitiable model.
 *
 * @mixin \Eloquent
 */
abstract class ActivitiableModel extends ResourcefulModel
{
    /**
     * The attributes that should be logged as activity additions.
     *
     * @var array
     */
    public static $additions = [];
}
