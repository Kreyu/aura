<?php

namespace Aura\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static Builder query()
 */
trait EloquentHelpers
{
    /**
     * Return model's table name.
     *
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    /**
     * Get collection of random entities of given quantity from model's table.
     *
     * @param  int $quantity
     * @return Collection|static[]
     */
    public static function draw($quantity = 5)
    {
        return self::query()
            ->inRandomOrder()
            ->limit($quantity)
            ->get();
    }

    /**
     * Get one random entity from model's table.
     *
     * @return Model|object|static|null
     */
    public static function drawOne()
    {
        return self::query()
            ->inRandomOrder()
            ->first();
    }
}
