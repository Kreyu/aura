<?php

namespace Aura\Database\Traits;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * @property array $dates
 */
trait TimezoneAware
{
    /**
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {
        $value = parent::__get($key);

        if ($this->isApplicable($key, $value)) {
            return $this->applyTimezone($value);
        }

        return $value;
    }

    /**
     * Determine if requested value can handle
     * the timezone modifications.
     *
     * @param  mixed $key
     * @param  mixed $value
     * @return bool
     */
    private function isApplicable($key, $value)
    {
        return property_exists($this, 'dates')
            && in_array($key, $this->dates)
            && $value instanceof Carbon;
    }

    /**
     * Apply timezone to model's dates.
     *
     * @param  Carbon $value
     * @return Carbon
     */
    protected function applyTimezone(Carbon $value)
    {
        return $value->timezone($this->getTimezone());
    }

    /**
     * Get timezone to apply to model's dates.
     *
     * @return string
     */
    protected function getTimezone()
    {
        return optional(Auth::user())->timezone ?: config('app.timezone');
    }
}
