<?php

namespace Aura;

use Aura\Contracts\ActivitySubject;
use Aura\Database\Eloquent\ActivitiableModel;
use Aura\Database\Traits\LogsActivity;
use Aura\Http\Resources\Project as ProjectResource;
use Aura\Http\Resources\ProjectCollection;
use Aura\Scopes\LatestScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Project model.
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int $identifier
 * @property string|null $homepage
 * @property bool $is_public
 * @property bool $inherits_members
 * @property string|null $slack_channel
 * @property string|null $slack_channel_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Issue[] $issues
 * @property-read Collection|User[] $participants
 * @property-read Collection|Activity[] $activities
 * @mixin \Eloquent
 */
class Project extends ActivitiableModel implements ActivitySubject
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The relationship counts that should be eager loaded on every query.
     *
     * @var array
     */
    protected $withCount = ['issues'];

    /**
     * The resource class of the model.
     *
     * @var string
     */
    protected $resource = ProjectResource::class;

    /**
     * The collection class of the model.
     *
     * @var string
     */
    protected $collection = ProjectCollection::class;

    /**
     * The alias used in polymorphic relations.
     *
     * @var string
     */
    public static $alias = 'projects';

    /**
     * The attributes that should be logged as activity additions.
     *
     * @var array
     */
    public static $additions = [
        'name', 'description', 'homepage', 'identifier',
        'slack_channel', 'slack_channel_id'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LatestScope);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'identifier';
    }

    /**
     * Get the issues created in the project.
     *
     * @return HasMany
     */
    public function issues()
    {
        return $this->hasMany(Issue::class);
    }

    /**
     * Get the users participating in the project.
     *
     * @return HasMany
     */
    public function participants()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Get the activities logged for the project.
     *
     * @return MorphMany
     */
    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }
}
